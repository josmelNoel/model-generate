<?php

namespace Cuviko\EloquentModelGenerator\Console;

use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Way\Generators\Commands\GeneratorCommand;
use Way\Generators\Generator;
use Way\Generators\Filesystem\Filesystem;
use Way\Generators\Compilers\TemplateCompiler;
use Illuminate\Contracts\Config\Repository as Config;

class GenerateModelsCommand extends GeneratorCommand {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'models:generate';
    private static $namespace;
    protected $timestampRules = 'ends:_at'; //['ends' => ['_at']];
    protected $fillableRules = '';
    protected $guardedRules = 'ends:_guarded'; //['ends' => ['_id', 'ids'], 'equals' => ['id']];
    protected $ruleProcessor;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Eloquent models from an existing table structure.';
    protected $extends = 'Illuminate\Database\Eloquent\Model';
    protected $databaseEngine = 'mysql';
    private $schemaGenerator;

    /**
     * @param Generator  $generator
     * @param Filesystem  $file
     * @param TemplateCompiler  $compiler
     * @param Config  $config
     */
    public function __construct(
    Generator $generator, Filesystem $file, TemplateCompiler $compiler, Config $config
    ) {
        $this->file = $file;
        $this->compiler = $compiler;
        $this->config = $config;

        parent::__construct($generator);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            ['tables', InputArgument::OPTIONAL, 'A list of Tables you wish to Generate Migrations for separated by a comma: users,posts,comments'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        //shameless copy
        return [
            ['connection', 'c', InputOption::VALUE_OPTIONAL, 'The database connection to use.', $this->config->get('database.default')],
            ['tables', 't', InputOption::VALUE_OPTIONAL, 'A list of Tables you wish to Generate Migrations for separated by a comma: users,posts,comments'],
            ['timestamps', null, InputOption::VALUE_OPTIONAL, 'Rules for $timestamps columns', $this->timestampRules],
            ['fillable', null, InputOption::VALUE_OPTIONAL, 'Rules for $fillable array columns', $this->fillableRules],
            ['guarded', null, InputOption::VALUE_OPTIONAL, 'Rules for $guarded array columns', $this->guardedRules],
            ['path', 'p', InputOption::VALUE_OPTIONAL, 'Where should the file be created?'],
            ['extends', null, InputOption::VALUE_OPTIONAL, 'Parent class', $this->extends],
            ['namespace', 'ns', InputOption::VALUE_OPTIONAL, 'Explicitly set the namespace'],
            ['overwrite', 'o', InputOption::VALUE_NONE, 'Overwrite existing models ?'],
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        //0. determine destination folder
        $destinationFolder = $this->getFileGenerationPath();

        //1. fetch all tables
        $this->info("\nFetching tables...");
        $this->initializeSchemaGenerator();
        $tables = $this->getTables();

        //2. for each table, fetch primary and foreign keys
        $this->info('Fetching table columns, primary keys, foreign keys');
        $prep = $this->getColumnsPrimaryAndForeignKeysPerTable($tables);

        //3. create an array of rules, holding the info for our Eloquent models to be
        $this->info('Generating Eloquent rules');


        $eloquentRules = $this->getEloquentRules($tables, $prep);

        //4. Generate our Eloquent Models

        $this->info("Generating Eloquent models\n");
        $this->generateEloquentModels($destinationFolder, $eloquentRules);

        $this->info("\nAll done!");
    }

    public function getTables() {
        $schemaTables = $this->schemaGenerator->getTables();

        $specifiedTables = $this->option('tables');

        //when no tables specified, generate all tables
        if (empty($specifiedTables)) {
            return $schemaTables;
        }

        $specifiedTables = explode(',', $specifiedTables);


        $tablesToGenerate = [];
        foreach ($specifiedTables as $specifiedTable) {
            if (!in_array($specifiedTable, $schemaTables)) {
                $this->error("specified table not found: $specifiedTable");
            } else {
                $tablesToGenerate[$specifiedTable] = $specifiedTable;
            }
        }

        if (empty($tablesToGenerate)) {
            $this->error('No tables to generate');
            die;
        }

        return array_values($tablesToGenerate);
    }

    private function generateEloquentModels($destinationFolder, $eloquentRules) {
        //0. set namespace
        self::$namespace = $this->getNamespace();
        foreach ($eloquentRules as $table => $rules) {
            try {
                $this->generateEloquentModel($destinationFolder, $table, $rules);
            } catch (Exception $e) {
                $this->error("\nFailed to generate model for table $table");
                return;
            }
        }
    }

    public function convertArrayToString($array) {
        $string = '';
        if (!empty($array)) {
            $string .= "\n        ";
            $string .= implode(",\n        ", $array);
            $string .= "\n    ";
        }
        return $string;
    }

    private function generateEloquentModel($destinationFolder, $table, $rules) {
        $extends = $this->option('extends');
        //1. Determine path where the file should be generated
        $modelName = $this->generateModelNameFromTableName($table);
        $filePathToGenerate = $destinationFolder . '/' . $modelName . '.php';

        $canContinue = $this->canGenerateEloquentModel($filePathToGenerate, $table);
        if (!$canContinue) {
            return;
        }

        //2.  generate relationship functions and fillable array
        $hasMany = $rules['hasMany'];
        $hasOne = $rules['hasOne'];
        $belongsTo = $rules['belongsTo'];
        $belongsToMany = $rules['belongsToMany'];

        $fillable = $this->convertArrayToString($rules['fillable']);
        $timestams = ($rules['timestamps']) ? 'true' : 'false';
        $belongsToFunctions = $this->generateBelongsToFunctions($belongsTo);
        $belongsToManyFunctions = $this->generateBelongsToManyFunctions($belongsToMany);
        $hasManyFunctions = $this->generateHasManyFunctions($hasMany);
        $hasOneFunctions = $this->generateHasOneFunctions($hasOne);

        $functions = $this->generateFunctions([
            $belongsToFunctions,
            $belongsToManyFunctions,
            $hasManyFunctions,
            $hasOneFunctions,
        ]);
        //3. prepare template data

        $shortNameExtends = explode('\\', $extends)[count(explode('\\', $extends)) - 1];
        $primaryKey = ( $rules['primaryKey'] ) ? ('protected $primaryKey = \'' . $rules['primaryKey'] . '\';' . "\r\t") : '';
        $templateData = array(
            'NAMESPACE' => self::$namespace,
            'NAME' => $modelName,
            'TABLENAME' => $table,
            'FILLABLE' => $fillable,
            'TIMESTAMPS' => $timestams,
            'GUARDED' => '',
            'FUNCTIONS' => $functions,
            'extends' => $extends,
            'shortNameExtends' => $shortNameExtends,
            'primaryKey' => $primaryKey
        );

        $templatePath = $this->getTemplatePath();

        //run Jeffrey's generator
        $this->generator->make(
                $templatePath, $templateData, $filePathToGenerate
        );
        $this->info("Generated model for table $table");
    }

    private function canGenerateEloquentModel($filePathToGenerate, $table) {
        $canOverWrite = $this->option('overwrite');
        if (file_exists($filePathToGenerate)) {
            if ($canOverWrite) {
                $deleted = unlink($filePathToGenerate);
                if (!$deleted) {
                    $this->warn("Failed to delete existing model $filePathToGenerate");
                    return false;
                }
            } else {
                $this->warn("Skipped model generation, file already exists. (force using --overwrite) $table -> $filePathToGenerate");
                return false;
            }
        }

        return true;
    }

    private function getNamespace() {
        $ns = $this->option('namespace');
        if (empty($ns)) {
            $ns = env('APP_NAME', 'App\Models');
        }

        //convert forward slashes in the namespace to backslashes
        $ns = str_replace('/', '\\', $ns);
        return $ns;
    }

    private function generateFunctions($functionsContainer) {
        $f = '';
        foreach ($functionsContainer as $functions) {
            $f .= $functions;
        }

        return $f;
    }

    private function generateHasManyFunctions($rulesContainer) {
        $functions = '';
        foreach ($rulesContainer as $rules) {
            $hasManyModel = $this->generateModelNameFromTableName($rules[0]);
            $key1 = $rules[1];
            $key2 = $rules[2];

            $hasManyFunctionName = $this->getPluralFunctionName($hasManyModel);

            $function = "
    public function $hasManyFunctionName() {" . '
        return $this->hasMany' . "(\\" . self::$namespace . "\\$hasManyModel::class, '$key1', '$key2');
    }
";
            $functions .= $function;
        }

        return $functions;
    }

    private function generateHasOneFunctions($rulesContainer) {
        $functions = '';
        foreach ($rulesContainer as $rules) {
            $hasOneModel = $this->generateModelNameFromTableName($rules[0]);
            $key1 = $rules[1];
            $key2 = $rules[2];

            $hasOneFunctionName = $this->getSingularFunctionName($hasOneModel);

            $function = "
    public function $hasOneFunctionName() {" . '
        return $this->hasOne' . "(\\" . self::$namespace . "\\$hasOneModel::class, '$key1', '$key2');
    }
";
            $functions .= $function;
        }

        return $functions;
    }

    private function generateBelongsToFunctions($rulesContainer) {
        $functions = '';
        foreach ($rulesContainer as $rules) {
            $belongsToModel = $this->generateModelNameFromTableName($rules[0]);
            $key1 = $rules[1];
            $key2 = $rules[2];

            $belongsToFunctionName = $this->getSingularFunctionName($belongsToModel);

            $function = "
    public function $belongsToFunctionName() {" . '
        return $this->belongsTo' . "(\\" . self::$namespace . "\\$belongsToModel::class, '$key1', '$key2');
    }
";
            $functions .= $function;
        }

        return $functions;
    }

    private function generateBelongsToManyFunctions($rulesContainer) {
        $functions = '';
        foreach ($rulesContainer as $rules) {
            $belongsToManyModel = $this->generateModelNameFromTableName($rules[0]);
            $through = $rules[1];
            $key1 = $rules[2];
            $key2 = $rules[3];

            $belongsToManyFunctionName = $this->getPluralFunctionName($belongsToManyModel);

            $function = "
    public function $belongsToManyFunctionName() {" . '
        return $this->belongsToMany' . "(\\" . self::$namespace . "\\$belongsToManyModel::class, '$through', '$key1', '$key2');
    }
";
            $functions .= $function;
        }

        return $functions;
    }

    private function getPluralFunctionName($modelName) {
        $modelName = lcfirst($modelName);
        return str_plural($modelName);
    }

    private function getSingularFunctionName($modelName) {
        $modelName = lcfirst($modelName);
        return str_singular($modelName);
    }

    private function generateModelNameFromTableName($table) {
        return ucfirst(camel_case(str_singular($table)));
    }

    private function getColumnsPrimaryAndForeignKeysPerTable($tables) {
        $prep = [];
        foreach ($tables as $table) {
            //get foreign keys
            $foreignKeys = $this->schemaGenerator->getForeignKeyConstraints($table);

            //get primary keys
            $primaryKeys = $this->schemaGenerator->getPrimaryKeys($table);

            // get columns lists
            $__columns = $this->schemaGenerator->getSchema()->listTableColumns($table);
            $columns = [];
            foreach ($__columns as $col) {
                $columns[] = $col->toArray()['name'];
            }

            $prep[$table] = [
                'foreign' => $foreignKeys,
                'primary' => $primaryKeys,
                'columns' => $columns,
            ];
        }

        return $prep;
    }

    private function getEloquentRules($tables, $prep) {
        $rules = [];
        foreach ($prep as $table => $properties) {
            $rules[$table] = [
                'hasMany' => [],
                'hasOne' => [],
                'belongsTo' => [],
                'belongsToMany' => [],
                'fillable' => [],
                'timestamps' => false,
                'guarded' => [],
                'primaryKey' => null
            ];
        }


        foreach ($prep as $table => $properties) {
            $foreign = $properties['foreign'];
            $primary = $properties['primary'];
            $columns = $properties['columns'];

            $this->setPrimarykeyProperties($table, $rules, $columns);
            $this->setTimestampsProperties($table, $rules, $columns);
            $this->setFillableProperties($table, $rules, $columns);

            $isManyToMany = $this->detectManyToMany($prep, $table);

            if ($isManyToMany === true) {
                $this->addManyToManyRules($tables, $table, $prep, $rules);
            }

            //the below used to be in an ELSE clause but we should be as verbose as possible
            //when we detect a many-to-many table, we still want to set relations on it
            //else
            {
                foreach ($foreign as $fk) {
                    $isOneToOne = $this->detectOneToOne($fk, $primary);

                    if ($isOneToOne) {
                        $this->addOneToOneRules($tables, $table, $rules, $fk);
                    } else {
                        $this->addOneToManyRules($tables, $table, $rules, $fk);
                    }
                }
            }
        }
        return $rules;
    }

    private function setPrimarykeyProperties($table, &$rules) {
        $primaryKey = $this->getTablePrimaryKey($table);
        $primaryKey = $primaryKey != 'id' ? $primaryKey : null;
        $rules[$table]['primaryKey'] = $primaryKey;
    }

    private function setFillableProperties($table, &$rules, $columns) {
        $fillable = [];
        foreach ($columns as $column_name) {
            if ($column_name !== 'created_at' && $column_name !== 'updated_at' && $column_name !== 'deleted_at') {
                $fillable[] = "'$column_name'";
            }
        }
        $rules[$table]['fillable'] = $fillable;
    }

    private function setTimestampsProperties($table, &$rules, $columns) {
        $timestamps = false;
        foreach ($columns as $column_name) {
            if ($this->check($this->option('timestamps'), $column_name)) {
                $timestamps = true;
            }
        }
        $rules[$table]['timestamps'] = $timestamps;
    }

    /**
     * Get table primary key column.
     *
     * @param $table
     *
     * @return string
     */
    protected function getTablePrimaryKey($table) {
        switch ($this->databaseEngine) {
            case 'mysql':
                $primaryKeyResult = \DB::select(
                                "SELECT COLUMN_NAME
                  FROM information_schema.COLUMNS 
                  WHERE  TABLE_SCHEMA = '" . env("DB_DATABASE") . "' AND 
                         TABLE_NAME = '{$table}' AND 
                         COLUMN_KEY = 'PRI'");
                break;

            case 'sqlsrv':
            case 'dblib':

                $primaryKeyResult = \DB::select(
                                "SELECT ku.COLUMN_NAME
                   FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS tc
                   INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS ku
                   ON tc.CONSTRAINT_TYPE = 'PRIMARY KEY' 
                   AND tc.CONSTRAINT_NAME = ku.CONSTRAINT_NAME
                   WHERE ku.TABLE_CATALOG ='" . env("DB_DATABASE") . "' AND ku.TABLE_NAME='{$table}';");
                break;

            case 'pgsql':

                $primaryKeyResult = \DB::select(
                                "SELECT ku.COLUMN_NAME AS \"COLUMN_NAME\"
                   FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS tc
                   INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS ku
                   ON tc.CONSTRAINT_TYPE = 'PRIMARY KEY' 
                   AND tc.CONSTRAINT_NAME = ku.CONSTRAINT_NAME
                   WHERE ku.TABLE_CATALOG ='" . env("DB_DATABASE") . "' AND ku.TABLE_NAME='{$table}';");
                break;
        }

        if (count($primaryKeyResult) == 1) {
            return $primaryKeyResult[0]->COLUMN_NAME;
        }

        return null;
    }

    private function check($rules, $value) {
        if (empty($rules)) {
            return true;
        }
        $value = strtolower($value);
        $rules = $this->parseRules($rules);
        foreach ($rules as $rule => $options) {
//            if (method_exists($this, $rule)) {
            $passed = $this->{$rule}($options, $value);
            if ($passed) {
                return true;
            }
//            } else {
//                throw new InvalidRuleException('Rule '.$rule.' not implemented');
//            }
        }
        return false;
    }

    public function ends($options, $value) {
        foreach ($options as $option) {
            $passed = (substr($value, -strlen($option)) === $option);

            if ($passed) {
                return true;
            }
        }

        return false;
    }

    private function parseRules($rules) {
        $groups = [];
        if (!empty($rules)) {
            $rules = str_replace(' ', '', $rules);
            $split = explode(',', $rules); //'ends:_id|ids,equals:id'
            foreach ($split as $rule) {
                list($type, $filters) = explode(':', $rule);
                $groups[$type] = explode('|', $filters);
            }
        }

        return $groups;
    }

    private function addOneToManyRules($tables, $table, &$rules, $fk) {
        //$table belongs to $FK
        //FK hasMany $table

        $fkTable = $fk['on'];
        $field = $fk['field'];
        $references = $fk['references'];
        if (in_array($fkTable, $tables)) {
            $rules[$fkTable]['hasMany'][] = [$table, $field, $references];
        }
        if (in_array($table, $tables)) {
            $rules[$table]['belongsTo'][] = [$fkTable, $field, $references];
        }
    }

    private function addOneToOneRules($tables, $table, &$rules, $fk) {
        //$table belongsTo $FK
        //$FK hasOne $table

        $fkTable = $fk['on'];
        $field = $fk['field'];
        $references = $fk['references'];
        if (in_array($fkTable, $tables)) {
            $rules[$fkTable]['hasOne'][] = [$table, $field, $references];
        }
        if (in_array($table, $tables)) {
            $rules[$table]['belongsTo'][] = [$fkTable, $field, $references];
        }
    }

    private function addManyToManyRules($tables, $table, $prep, &$rules) {

        //$FK1 belongsToMany $FK2
        //$FK2 belongsToMany $FK1

        $foreign = $prep[$table]['foreign'];

        $fk1 = $foreign[0];
        $fk1Table = $fk1['on'];
        $fk1Field = $fk1['field'];
        //$fk1References = $fk1['references'];

        $fk2 = $foreign[1];
        $fk2Table = $fk2['on'];
        $fk2Field = $fk2['field'];
        //$fk2References = $fk2['references'];
        //User belongstomany groups user_group, user_id, group_id
        if (in_array($fk1Table, $tables)) {
            $rules[$fk1Table]['belongsToMany'][] = [$fk2Table, $table, $fk1Field, $fk2Field];
        }
        if (in_array($fk2Table, $tables)) {
            $rules[$fk2Table]['belongsToMany'][] = [$fk1Table, $table, $fk2Field, $fk1Field];
        }
    }

    //if FK is also a primary key, and there is only one primary key, we know this will be a one to one relationship
    private function detectOneToOne($fk, $primary) {
        if (count($primary) === 1) {
            foreach ($primary as $prim) {
                if ($prim === $fk['field']) {
                    return true;
                }
            }
        }

        return false;
    }

    //does this table have exactly two foreign keys that are also NOT primary,
    //and no tables in the database refer to this table?
    private function detectManyToMany($prep, $table) {
        $properties = $prep[$table];
        $foreignKeys = $properties['foreign'];
        $primaryKeys = $properties['primary'];

        //ensure we only have two foreign keys
        if (count($foreignKeys) === 2) {

            //ensure our foreign keys are not also defined as primary keys
            $primaryKeyCountThatAreAlsoForeignKeys = 0;
            foreach ($foreignKeys as $foreign) {
                foreach ($primaryKeys as $primary) {
                    if ($primary === $foreign['name']) {
                        ++$primaryKeyCountThatAreAlsoForeignKeys;
                    }
                }
            }

            if ($primaryKeyCountThatAreAlsoForeignKeys === 1) {
                //one of the keys foreign keys was also a primary key
                //this is not a many to many. (many to many is only possible when both or none of the foreign keys are also primary)
                return false;
            }

            //ensure no other tables refer to this one
            foreach ($prep as $compareTable => $properties) {
                if ($table !== $compareTable) {
                    foreach ($properties['foreign'] as $prop) {
                        if ($prop['on'] === $table) {
                            return false;
                        }
                    }
                }
            }
            //this is a many to many table!
            return true;
        }

        return false;
    }

    private function initializeSchemaGenerator() {
        $this->schemaGenerator = new SchemaGenerator(
                $this->option('connection'), null, null
        );

        return $this->schemaGenerator;
    }

    /**
     * Fetch the template data.
     *
     * @return array
     */
    protected function getTemplateData() {
        return [
            'NAME' => ucwords($this->argument('modelName')),
            'NAMESPACE' => env('APP_NAME', 'App\Models'),
        ];
    }

    /**
     * The path to where the file will be created.
     *
     * @return mixed
     */
    protected function getFileGenerationPath() {
        $path = $this->getPathByOptionOrConfig('path', 'model_target_path');

        if (!is_dir($path)) {
            $this->warn('Path is not a directory, creating ' . $path);
            mkdir($path);
        }

        return $path;
    }

    /**
     * Get the path to the generator template.
     *
     * @return mixed
     */
    protected function getTemplatePath() {
        $tp = __DIR__ . '/templates/model.txt';

        return $tp;
    }

}
